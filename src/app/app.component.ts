import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'demo ';
  bgColor : string = '';
  onChange(searchValue: string): void {  
    console.log(searchValue);
    this.bgColor = searchValue;
  }






}